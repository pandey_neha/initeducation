<!-- BEGIN BREADCRUMBS -->
<div class="row breadcrumbs margin-bottom-40">
    <div class="container">
        <div class="col-md-4 col-sm-4">
            <h1>Search Results</h1>
        </div>
        <div class="col-md-8 col-sm-8">
            <ul class="pull-right breadcrumb">
                <li><!--<a href="index.html">Home</a>-->
                    <?php echo $this->html->link('Home', array('controller' => '', 'action' => 'index')); ?>
                </li>
                <li><!--<a href="">Pages</a>-->
                    <?php echo $this->html->link('Pages', array('controller' => '', 'action' => '')); ?>
                </li>
                <li class="active">Search Results</li>
            </ul>
        </div>
    </div>
</div>
<!-- END BREADCRUMBS -->

<!-- BEGIN CONTAINER -->
<div class="container min-hight">
    <div class="row margin-bottom-40">
        <div class="col-md-12 col-sm-12">
            <div class="row search-form-default">
                <?php echo $this->Form->create('', array('class' => 'form-inline')) ?>
                <div class="input-group">
                    <div class="input-cont">
                        <?php echo $this->Form->input('', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Search..',
                            'label' => false, 'div' => false)); ?>
                    </div>
                                    <span class="input-group-btn">
                           <?php echo $this->Form->button('Send.<i class="m-icon-swapright m-icon-white"></i>',
                               array('class' => 'btn btn-default theme-btn', 'type' => 'submit', 'label' => false, 'div' => false)) ?>
                                    </span>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Metronic - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Metronic - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin
                    and backend applications. Metronic has a clean and intuitive metro style design which makes your
                    next project look awesome and yet user friendly..</p>
                <!--<a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Conquer - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Conquer - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Conquer is a responsive admin dashboard template created mainly for admin and backend
                    applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with
                    Twitter Bootstrap Framework..</p>
                <!-- <a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Metronic - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Metronic - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin
                    and backend applications. Metronic has a clean and intuitive metro style design which makes your
                    next project look awesome and yet user friendly..</p>
                <!--<a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Conquer - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Conquer - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Conquer is a responsive admin dashboard template created mainly for admin and backend
                    applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with
                    Twitter Bootstrap Framework..</p>
                <!--<a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Conquer - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Conquer - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Conquer is a responsive admin dashboard template created mainly for admin and backend
                    applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with
                    Twitter Bootstrap Framework..</p>
                <!--   <a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Metronic - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Metronic - Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Metronic is a responsive admin dashboard template powered with Twitter Bootstrap Framework for admin
                    and backend applications. Metronic has a clean and intuitive metro style design which makes your
                    next project look awesome and yet user friendly..</p>
                <!--  <a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="search-result-item">
                <h4><!--<a href="#">Conquer - Responsive Admin Dashboard Template</a>-->
                    <?php echo $this->html->link('Conquer- Responsive Admin Dashboard Template', array('controller' => '', 'action' => '')); ?>
                </h4>

                <p>Conquer is a responsive admin dashboard template created mainly for admin and backend
                    applications(CMS, CRM, Custom Admin Application, Admin Dashboard). Conquer template powered with
                    Twitter Bootstrap Framework..</p>
                <!--<a href="#" class="search-link">http://www.keenthemes.com</a>-->
                <?php echo $this->html->link('http://www.keenthemes.com', array('controller' => '', 'action' => ''), array('class' => 'search-link')); ?>
            </div>
            <div class="margin-top-20">
                <ul class="pagination">
                    <li><!--<a href="#">Prev</a>-->
                        <?php echo $this->html->link('Prev', array('controller' => '', 'action' => '')); ?>

                    </li>
                    <li><!--<a href="#">1</a>-->
                        <?php echo $this->html->link('1', array('controller' => '', 'action' => '')); ?>
                    </li>
                    <li><!--<a href="#">2</a>-->
                        <?php echo $this->html->link('2', array('controller' => '', 'action' => '')); ?>
                    </li>
                    <li class="active"><!--<a href="#">3</a>-->
                        <?php echo $this->html->link('3', array('controller' => '', 'action' => '')); ?>

                    </li>
                    <li><!--<a href="#">4</a>-->
                        <?php echo $this->html->link('4', array('controller' => '', 'action' => '')); ?>
                    </li>
                    <li><!--<a href="#">5</a>-->
                        <?php echo $this->html->link('5', array('controller' => '', 'action' => '')); ?>
                    </li>
                    <li><!--<a href="#">Next</a>-->
                        <?php echo $this->html->link('Next', array('controller' => '', 'action' => '')); ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
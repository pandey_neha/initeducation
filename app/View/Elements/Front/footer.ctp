<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 space-mobile">
                <!-- BEGIN ABOUT -->
                <h2>About</h2>

                <p class="margin-bottom-30">Vivamus imperdiet felis consectetur onec eget orci adipiscing nunc.
                    Pellentesque fermentum, ante ac interdum ullamcorper.</p>

                <div class="clearfix"></div>
                <!-- END ABOUT -->

                <h2>Photos Stream</h2>
                <!-- BEGIN BLOG PHOTOS STREAM -->
                <div class="blog-photo-stream margin-bottom-30">
                    <ul class="list-unstyled">
                        <li><!--<a href="#"><img src="/front/assets/img/people/img5-small.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/people/img5-small.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/works/img1.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/works/img1.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/people/img4-large.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/people/img4-large.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/works/img6.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/works/img6.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/pics/img1-large.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/pics/img1-large.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/pics/img2-large.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/pics/img2-large.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/works/img3.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/works/img3.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/people/img2-large.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/people/img2-large.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/works/img2.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/works/img2.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                        <li><!--<a href="#"><img src="assets/img/works/img5.jpg" alt=""></a>-->
                            <?php echo $this->html->link($this->html->image('/front/assets/img/works/img5.jpg', array('alt' => '')),
                                array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        </li>
                    </ul>
                </div>
                <!-- END BLOG PHOTOS STREAM -->
            </div>
            <div class="col-md-4 col-sm-4 space-mobile">
                <!-- BEGIN CONTACTS -->
                <h2>Contact Us</h2>
                <address class="margin-bottom-40">
                    Loop, Inc. <br/>
                    795 Park Ave, Suite 120 <br/>
                    San Francisco, CA 94107 <br/>
                    P: (234) 145-1810 <br/>
                    Email: <!--<a href="mailto:info@keenthemes.com">info@keenthemes.com</a>-->
                    <?php echo $this->html->link('info@keenthemes.com', array('controller' => '', 'action' => '')); ?>
                </address>
                <!-- END CONTACTS -->

                <!-- BEGIN SUBSCRIBE -->
                <h2>Monthly Newsletter</h2>

                <p>Subscribe to our newsletter and stay up to date with the latest news and deals!</p>
                <!--<form action="#" class="form-subscribe">
                    <div class="input-group input-large">
                        <input class="form-control" type="text">
                            <span class="input-group-btn">
                                <button class="btn theme-btn" type="button">SUBSCRIBE</button>
                            </span>
                    </div>
                </form>-->
                <?php echo $this->Form->create('', array('class' => 'form-subscribe')); ?>
                <div class="input-group input-large">
                    <?php echo $this->Form->input('', array('type' => 'text', 'class' => 'form-control', 'div' => false)); ?>
                    <span class="input-group-btn">
                        <?php echo $this->Form->input('SUBSCRIBE', array('type' => 'button', 'class' => 'btn theme-btn', 'label' => false, 'div' => false)); ?>
                        </span>
                </div>
                <?php echo $this->Form->end(); ?>


                <!-- END SUBSCRIBE -->
            </div>
            <div class="col-md-4 col-sm-4">
                <!-- BEGIN TWITTER BLOCK -->
                <h2>Latest Tweets</h2>
                <dl class="dl-horizontal f-twitter">
                    <dt>
                        <!--<i class="icon-twitter"></i>-->
                        <?php echo $this->html->tag('i', '', array('class' => 'icon-twitter')); ?>
                    </dt>
                    <dd>
                        <!-- <a href="#">@keenthemes</a>-->
                        <?php echo $this->html->link('@keenthemes', array('controller' => '', 'action' => '')); ?>
                        Imperdiet condimentum diam dolor lorem sit consectetur adipiscing
                        <span>3 min ago</span>

                    </dd>
                </dl>
                <dl class="dl-horizontal f-twitter">
                    <dt>
                        <!--<i class="icon-twitter"></i>-->
                        <?php echo $this->html->tag('i', '', array('class' => 'icon-twitter')); ?>
                    </dt>
                    <dd>
                        <!-- <a href="#">@keenthemes</a>-->
                        <?php echo $this->html->link('@keenthemes', array('controller' => '', 'action' => '')); ?>
                        Sequat ipsum dolor onec eget orci fermentum condimentum lorem sit consectetur adipiscing
                        <span>8 min ago</span>
                    </dd>
                </dl>
                <dl class="dl-horizontal f-twitter">
                    <dt>
                        <!--<i class="icon-twitter"></i>-->
                        <?php echo $this->html->tag('i', '', array('class' => 'icon-twitter')); ?>
                    </dt>
                    <dd>
                        <!--<a href="#">@keenthemes</a> -->
                        <?php echo $this->html->link('@keenthemes', array('controller' => '', 'action' => '')); ?>
                        Remonde sequat ipsum dolor lorem sit consectetur adipiscing
                        <span>12 min ago</span>
                    </dd>
                </dl>
                <dl class="dl-horizontal f-twitter">
                    <dt>
                        <?php echo $this->html->tag('i', '', array('class' => 'icon-twitter')); ?>
                    </dt>
                    <dd>
                        <?php echo $this->html->link('@keenthemes', array('controller' => '', 'action' => '')); ?>
                        Pilsum dolor lorem sit consectetur adipiscing orem sequat
                        <span>16 min ago</span>
                    </dd>
                </dl>
                <!-- END TWITTER BLOCK -->
            </div>
        </div>
    </div>
</div>
<!-- END FOOTER -->

<div class="copyright">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <p>
                    <!--<span class="margin-right-10">2013 © Metronic. ALL Rights Reserved.</span>-->
                    <?php echo $this->Html->tag('span', '2013 © Metronic. ALL Rights Reserved.', array('class' => 'margin-right-10')) ?>
                    <?php echo $this->Html->link('Privacy Policy', array('controller' => '', 'action' => '')) ?>
                    |<?php echo $this->Html->link('Terms of Service', array('controller' => '', 'action' => '')); ?>
                </p>
            </div>
            <div class="col-md-4 col-sm-4">
                <ul class="social-footer">
                    <li>
                        <!--                    <<a href="#"><i class="icon-facebook"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-facebook')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-google-plus"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-google-plus')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-dribbble"></i>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-dribbble')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-linkedin"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-linkedin')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-twitter"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-twitter')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-skype"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-skype')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-github"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-github')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-youtube"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-youtube')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                    <li><!--<a href="#"><i class="icon-dropbox"></i></a>-->
                        <?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'icon-dropbox')), array('controller' => '', 'action' => ''), array('escape' => false)) ?>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- END COPYRIGHT -->

<div class="header navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <button class="navbar-toggle btn navbar-btn" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- END RESPONSIVE MENU TOGGLER -->
            <!-- BEGIN LOGO (you can use logo image instead of text)-->
            <!--<a class="navbar-brand logo-v1" href="index.html">
                <img src="assets/img/logo_blue.png" id="logoimg" alt="">
            </a>-->
          <?php echo $this->html->link($this->html->image('/front/assets/img/logo_blue.png',array('alt'=>'')),
              array('controller'=>'','action'=>'index'),array('class'=>'navbar-brand logo-v1','escape'=>false))?>
            <!-- END LOGO -->
        </div>

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="dropdown active">
                    <!-- <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                         Home
                         <i class="icon-angle-down"></i>
                     </a>-->
                    <?php echo $this->Html->link('Home' . $this->Html->tag('i', '', array('class' => 'icon-angle-down')),
                        array('controller' => '', 'action' => ''), array('escape' => false, 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown', 'data-delay' => '0', 'data-close-others' => 'false'))
                    ?>
                    <ul class="dropdown-menu">
                        <li class="active">
                            <!--<a href="index.html">Home Default</a>-->
                            <?php echo $this->Html->link('Home Default', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!--<a href="page_home2.html">Home with Top Bar</a>-->
                            <?php echo $this->Html->link('Home with Top Bar', array('controller' => '', 'action' => '')); ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <!-- <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                         Pages
                         <i class="icon-angle-down"></i>
                     </a>-->
                    <?php echo $this->Html->link('Pages' . $this->Html->tag('i', '', array('class' => 'icon-angle-down')),
                        array('controller' => '', 'action' => ''), array('escape' => false, 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown', 'data-delay' => '0', 'data-close-others' => 'false'))
                    ?>
                    <ul class="dropdown-menu">
                        <li>
                            <!--<a href="page_about.html">About Us</a>-->
                            <?php echo $this->Html->link('About Us', array('controller' => '', 'action' => '')); ?>

                        </li>
                        <li>
                            <!-- <a href="page_services.html">Services</a>-->
                            <?php echo $this->Html->link('Services', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li>
                            <!-- <a href="page_prices.html">Prices</a>-->
                            <?php echo $this->Html->link('Prices', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li><!--<a href="page_faq.html">FAQ</a>-->
                            <?php echo $this->Html->link('FAQ', array('controller' => '', 'action' => '')); ?>
                        </li>

                        <li><!--<a href="page_gallery.html">Gallery</a>-->
                            <?php echo $this->Html->link('Gallery', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li><!--<a href="page_search_result.html">Search Result</a>-->
                            <?php echo $this->Html->link('Search Results', array('controller' => 'searchs', 'action' => 'index')); ?>
                        </li>
                        <li><!--<a href="page_404.html">404</a>-->
                            <?php echo $this->Html->link('404', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li><!--<a href="page_500.html">500</a>-->
                            <?php echo $this->Html->link('500', array('controller' => '', 'action' => '')); ?>
                        </li>
                        <li> <!--<a href="page_contacts.html">Contact</a>-->
                            <?php echo $this->Html->link('Contact', array('controller' => '', 'action' => '')); ?>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <!--<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Features
                        <i class="icon-angle-down"></i>
                    </a>-->
                    <?php echo $this->Html->link('Features' . $this->Html->tag('i', '', array('class' => 'icon-angle-down')),
                        array('controller' => '', 'action' => ''), array('escape' => false, 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown', 'data-delay' => '0', 'data-close-others' => 'false'))
                    ?>

                    <ul class="dropdown-menu">
                        <li><!--<a href="feature_typography.html">Typography</a>-->
                            <?php echo $this->Html->link('Typography', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="feature_buttons.html">Buttons</a>-->
                            <?php echo $this->Html->link('Buttons', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="feature_forms.html">Forms</a>-->
                            <?php echo $this->Html->link('Forms', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="feature_icons.html">Icons</a>-->
                            <?php echo $this->Html->link('Icons', array('controller' => '', 'action' => '')); ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <!-- <a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                         Portfolio
                         <i class="icon-angle-down"></i>
                     </a>-->
                    <?php echo $this->Html->link('Portfolio' . $this->Html->tag('i', '', array('class' => 'icon-angle-down')),
                        array('controller' => '', 'action' => ''), array('escape' => false, 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown', 'data-delay' => '0', 'data-close-others' => 'false'))
                    ?>

                    <ul class="dropdown-menu">
                        <li><!--<a href="portfolio_4.html">Portfolio 4</a>-->
                            <?php echo $this->Html->link('Portfolio 4', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="portfolio_3.html">Portfolio 3</a>-->
                            <?php echo $this->Html->link('Portfolio 3', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="portfolio_2.html">Portfolio 2</a>-->
                            <?php echo $this->Html->link('Portfolio 2', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="portfolio_item.html">Portfolio Item</a>-->
                            <?php echo $this->Html->link('Portfolio Item', array('controller' => '', 'action' => '')); ?></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <!--<a class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false" href="#">
                        Blog
                        <i class="icon-angle-down"></i>
                    </a>-->
                    <?php echo $this->Html->link('Blog' . $this->Html->tag('i', '', array('class' => 'icon-angle-down')),
                        array('controller' => '', 'action' => ''), array('escape' => false, 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown', 'data-hover' => 'dropdown', 'data-delay' => '0', 'data-close-others' => 'false'))
                    ?>

                    <ul class="dropdown-menu">
                        <li><!--<a href="blog.html">Blog Page</a>-->
                            <?php echo $this->Html->link('Blog Page', array('controller' => '', 'action' => '')); ?></li>
                        <li><!--<a href="blog_item.html">Blog Item</a>-->
                            <?php echo $this->Html->link('Blog Item', array('controller' => '', 'action' => '')); ?></li>
                    </ul>
                </li>
                <li>
                    <!--<a href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin&page=index.html" target="_blank">Admin Theme</a>-->
                    <?php echo $this->Html->link('Admin Theme', array('controller' => '', 'action' => '')); ?></li>
                <li class="menu-search">
                    <span class="sep"></span>
                    <i class="icon-search search-btn"></i>

                    <div class="search-box">
                        <?php echo $this->Form->create(); ?>
                        <div class="input-group input-large">
                            <?php echo $this->Form->input('', array('class' => 'form-control', 'type' => 'text', 'placeholder' => "Search...", 'label' => false, 'div' => false)); ?>
                            <span class="input-group-btn">
                              <?php echo $this->Form->button('Go', array('type' => 'submit', 'class' => 'btn theme-btn', 'div' => false, 'label' => false)); ?>
                            </span>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </li>
            </ul>
        </div>
        <!-- end TOP NAVIGATION MENU -->
    </div>
</div>
<!-- END HEADER -->

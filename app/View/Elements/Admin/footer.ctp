<div class="footer">
    <div class="footer-inner">
        2013 &copy; Metronic by keenthemes.
    </div>
    <div class="footer-tools">
        <?php echo $this->Html->tag('span', '' . $this->Html->tag('i', '', array('class' => 'icon-angle-up')),
            array('class' => 'go-top'),
            array('escape' => false)); ?>
    </div>
</div>
<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
    <meta charset="utf-8"/>
    <title>Metronic Frotnend | Homepage</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <?php echo $this->html->css("/front/assets/plugins/font-awesome/css/font-awesome.min.css");
    echo $this->html->css("/front/assets/plugins/bootstrap/css/bootstrap.min.css"); ?>
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
    <?php echo $this->html->css("/front/assets/plugins/fancybox/source/jquery.fancybox.css");
    echo $this->html->css("/front/assets/plugins/revolution_slider/css/rs-style.css");
    echo $this->html->css("/front/assets/plugins/revolution_slider/rs-plugin/css/settings.css");
    echo $this->html->css("/front/assets/plugins/bxslider/jquery.bxslider.css"); ?>
    <?php echo $this->html->css("/front/assets/plugins/uniform/css/uniform.default.css"); ?>
    <!-- END PAGE LEVEL PLUGIN STYLES -->

    <!-- BEGIN THEME STYLES -->
    <?php echo $this->html->css("/front/assets/css/style-metronic.css");
    echo $this->html->css("/front/assets/css/style.css");
    echo $this->html->css("/front/assets/css/themes/blue.css");
    echo $this->html->css("/front/assets/css/style-responsive.css");
    echo $this->html->css("/front/assets/css/custom.css"); ?>
    <!-- END THEME STYLES -->

    <link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- END HEAD -->
<body>
<div>
    <?php echo $this->element('Front/header'); ?>
</div>
<div class="page-container">
    <div>
        <?php echo $this->fetch('content');?>
        </div >
    <?php echo $this->element('Front/footer'); ?>
        <div>
            <?php echo $this->element('Front/copyright'); ?>
        </div>
    </div>

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS(REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <?php echo $this->Html->script("/front/assets/plugins/respond.min.js");?>
    <![endif]-->
    <?php echo $this->Html->script("/front/assets/plugins/jquery-1.10.2.min.js");
    echo $this->Html->script("/front/assets/plugins/jquery-migrate-1.2.1.min.js");
    echo $this->Html->script("/front/assets/plugins/bootstrap/js/bootstrap.min.js");
    echo $this->Html->script("/front/assets/plugins/hover-dropdown.js");
    echo $this->Html->script("/front/assets/plugins/back-to-top.js"); ?>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS(REQUIRED ONLY FOR CURRENT PAGE) -->
    <?php echo $this->Html->script("/front/assets/plugins/fancybox/source/jquery.fancybox.pack.js");
    echo $this->Html->script("/front/assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.plugins.min.js");
    echo $this->Html->script("/front/assets/plugins/revolution_slider/rs-plugin/js/jquery.themepunch.revolution.min.js");
    echo $this->Html->script("/front/assets/plugins/bxslider/jquery.bxslider.min.js");
    echo $this->Html->script("/front/assets/scripts/app.js");
    echo $this->Html->script("/front/assets/scripts/index.js"); ?>
    <?php echo $this->html->script("/front/assets/scripts/app.js"); ?>
    <!--<script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
        });
    </script>-->
    <!-- END PAGE LEVEL JAVASCRIPTS -->

    <script type="text/javascript">
        jQuery(document).ready(function () {
            App.init();
            App.initBxSlider();
            Index.initRevolutionSlider();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->

</body>
<!-- END BODY -->

</html>
<div class="fullwidthbanner-container slider-main">
    <div class="fullwidthabnner">
        <ul id="revolutionul" style="display:none;">
            <!-- THE FIRST SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                data-thumb="/front/assets/img/sliders/revolution/thumbs/thumb2.jpg">
                <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                <!-- <img src="assets/img/sliders/revolution/bg1.jpg" alt="">-->
                <?php echo $this->html->image('/front/assets/img/sliders/revolution/bg1.jpg', array('alt' => '')); ?>

                <div class="caption lft slide_title slide_item_left"
                     data-x="0"
                     data-y="105"
                     data-speed="400"
                     data-start="1500"
                     data-easing="easeOutExpo">
                    Need a website design ?
                </div>
                <div class="caption lft slide_subtitle slide_item_left"
                     data-x="0"
                     data-y="180"
                     data-speed="400"
                     data-start="2000"
                     data-easing="easeOutExpo">
                    This is what you were looking for
                </div>
                <div class="caption lft slide_desc slide_item_left"
                     data-x="0"
                     data-y="220"
                     data-speed="400"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    Lorem ipsum dolor sit amet, dolore eiusmod<br>
                    quis tempor incididunt. Sed unde omnis iste.
                </div>
                <!-- <a class="caption lft btn green slide_btn slide_item_left" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes"
 -->                 <?php echo $this->html->link(' Purchase Now!', array('controller' => '', 'action' => 'http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes'
                ), array('class' => 'caption lft btn green slide_btn slide_item_left',
                    'data-x' => '0',
                    'data-y' => '290',
                    'data-speed' => '400',
                    'data-start' => '3000',
                    'data-easing' => 'easeOutExpo')); ?>

                <div class="caption lfb"
                     data-x="640"
                     data-y="55"
                     data-speed="700"
                     data-start="1000"
                     data-easing="easeOutExpo">
                    <!--   <img src="assets/img/sliders/revolution/man-winner.png" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/man-winner.png', array('alt' => 'Image 1')); ?>
                </div>
            </li>

            <!-- THE SECOND SLIDE -->
            <li data-transition="fade" data-slotamount="7" data-masterspeed="300" data-delay="9400"
                data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                <!--<img src="assets/img/sliders/revolution/bg2.jpg" alt="">-->
                <?php echo $this->html->image('/front/assets/img/sliders/revolution/bg2.jpg', array('alt' => '')); ?>
                <div class="caption lfl slide_title slide_item_left"
                     data-x="0"
                     data-y="125"
                     data-speed="400"
                     data-start="3500"
                     data-easing="easeOutExpo">
                    Powerfull & Clean
                </div>
                <div class="caption lfl slide_subtitle slide_item_left"
                     data-x="0"
                     data-y="200"
                     data-speed="400"
                     data-start="4000"
                     data-easing="easeOutExpo">
                    Responsive Admin & Website Theme
                </div>
                <div class="caption lfl slide_desc slide_item_left"
                     data-x="0"
                     data-y="245"
                     data-speed="400"
                     data-start="4500"
                     data-easing="easeOutExpo">
                    Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                </div>
                <div class="caption lfr slide_item_right"
                     data-x="635"
                     data-y="105"
                     data-speed="1200"
                     data-start="1500"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/mac.png" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/mac.png', array('alt' => 'Image 1')); ?>
                </div>
                <div class="caption lfr slide_item_right"
                     data-x="580"
                     data-y="245"
                     data-speed="1200"
                     data-start="2000"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/ipad.png" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/ipad.png', array('alt' => 'Image 1')); ?>
                </div>
                <div class="caption lfr slide_item_right"
                     data-x="735"
                     data-y="290"
                     data-speed="1200"
                     data-start="2500"
                     data-easing="easeOutBack">
                    <!--           <img src="assets/img/sliders/revolution/iphone.png" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/iphone.png', array('alt' => 'Image 1')); ?>
                </div>
                <div class="caption lfr slide_item_right"
                     data-x="835"
                     data-y="230"
                     data-speed="1200"
                     data-start="3000"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/macbook.png" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/macbook.png', array('alt' => 'Image 1')); ?>
                </div>
                <div class="caption lft slide_item_right"
                     data-x="865"
                     data-y="45"
                     data-speed="500"
                     data-start="5000"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/hint1-blue.png" id="rev-hint1" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/hint1-blue.png', array('alt' => 'Image 1')); ?>
                </div>
                <div class="caption lfb slide_item_right"
                     data-x="355"
                     data-y="355"
                     data-speed="500"
                     data-start="5500"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/hint2-blue.png" id="rev-hint2" alt="Image 1">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/hint2-blue.png', array('alt' => 'Image 1')); ?>
                </div>

            </li>

            <!-- THE THIRD SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                data-thumb="/front/assets/img/sliders/revolution/thumbs/thumb2.jpg">
                <!--<img src="assets/img/sliders/revolution/bg3.jpg" alt="">-->
                <?php echo $this->html->image('/front/assets/img/sliders/revolution/bg3.jpg', array('alt' => '')); ?>
                <div class="caption lfl slide_item_left"
                     data-x="20"
                     data-y="95"
                     data-speed="400"
                     data-start="1500"
                     data-easing="easeOutBack">
                    <iframe src="http://player.vimeo.com/video/56974716?portrait=0" width="420" height="240"
                            style="border:0" allowFullScreen></iframe>
                </div>
                <div class="caption lfr slide_title"
                     data-x="470"
                     data-y="100"
                     data-speed="400"
                     data-start="2000"
                     data-easing="easeOutExpo">
                    Responsive Video Support
                </div>
                <div class="caption lfr slide_subtitle"
                     data-x="470"
                     data-y="170"
                     data-speed="400"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    Youtube, Vimeo and others.
                </div>
                <div class="caption lfr slide_desc"
                     data-x="470"
                     data-y="220"
                     data-speed="400"
                     data-start="3000"
                     data-easing="easeOutExpo">
                    Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                </div>
                <!-- <a class="caption lfr btn yellow slide_btn" href=""
                    data-x="470"
                    data-y="280"
                    data-speed="400"
                    data-start="3500"
                    data-easing="easeOutExpo">
                     Watch more Videos!
                 </a>-->
                <?php echo $this->html->link('Watch more Videos!', array('controller' => '', 'action' => ''), array(
                    'class' => 'caption lfr btn yellow slide_btn',
                    'data-x' => '470',
                    'data-y' => '280',
                    'data-speed' => '400',
                    'data-start' => '3500',
                    'data-easing' => 'easeOutExpo'
                )) ?>
            </li>

            <!-- THE FORTH SLIDE -->
            <li data-transition="fade" data-slotamount="8" data-masterspeed="700" data-delay="9400"
                data-thumb="assets/img/sliders/revolution/thumbs/thumb2.jpg">
                <!-- THE MAIN IMAGE IN THE FIRST SLIDE -->
                <!--<img src="assets/img/sliders/revolution/bg4.jpg" alt="">-->
                <?php echo $this->html->image('/front/assets/img/sliders/revolution/bg4.jpg', array('alt' => '')); ?>
                <div class="caption lft slide_title"
                     data-x="0"
                     data-y="105"
                     data-speed="400"
                     data-start="1500"
                     data-easing="easeOutExpo">
                    What else included ?
                </div>
                <div class="caption lft slide_subtitle"
                     data-x="0"
                     data-y="180"
                     data-speed="400"
                     data-start="2000"
                     data-easing="easeOutExpo">
                    The Most Complete Admin Theme
                </div>
                <div class="caption lft slide_desc"
                     data-x="0"
                     data-y="225"
                     data-speed="400"
                     data-start="2500"
                     data-easing="easeOutExpo">
                    Lorem ipsum dolor sit amet, consectetuer elit sed diam<br> nonummy amet euismod dolore.
                </div>
                <!--<a class="caption lft slide_btn btn red slide_item_left" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin" target="_blank"
                   data-x="0"
                   data-y="300"
                   data-speed="400"
                   data-start="3000"
                   data-easing="easeOutExpo">
                    Learn More!
                </a>-->
                <?php echo $this->html->link('Learn More', array('cotroller' => '', 'action' => ''),
                    array('class' => 'caption lft slide_btn btn red slide_item_left',
                        'data-x' => '0',
                        'data-y' => '300',
                        'data-speed' => '400',
                        'data-start' => '3000',
                        'data-easing' => 'easeOutExpo'
                    )); ?>
                <div class="caption lft start"
                     data-x="670"
                     data-y="55"
                     data-speed="400"
                     data-start="2000"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/iphone_left.png" alt="Image 2">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/iphone_left.png', array('alt' => 'Image 2')); ?>
                </div>

                <div class="caption lft start"
                     data-x="850"
                     data-y="55"
                     data-speed="400"
                     data-start="2400"
                     data-easing="easeOutBack">
                    <!--<img src="assets/img/sliders/revolution/iphone_right.png" alt="Image 3">-->
                    <?php echo $this->html->image('/front/assets/img/sliders/revolution/iphone_right.png', array('alt' => 'Image 3')); ?>
                </div>
            </li>
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
</div>
<!-- END REVOLUTION SLIDER -->

<!-- BEGIN CONTAINER -->
<div class="container">
    <!-- BEGIN SERVICE BOX -->
    <div class="row service-box">
        <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
                <em><i class="icon-location-arrow blue"></i></em>
                <span>Multipurpose Template</span>
            </div>
            <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
                <em><i class="icon-ok red"></i></em>
                <span>Well Documented</span>
            </div>
            <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="service-box-heading">
                <em><i class="icon-resize-small green"></i></em>
                <span>Responsive Design</span>
            </div>
            <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde
                nostrudlaboris. Sed unde omnis iste natus error sit voluptatem.</p>
        </div>
    </div>
    <!-- END SERVICE BOX -->

    <!-- BEGIN BLOCKQUOTE BLOCK -->
    <div class="row quote-v1">
        <div class="col-md-9 quote-v1-inner">
            <span>Metronic - The Most Complete & Popular Admin & Frontend Theme</span>
        </div>
        <div class="col-md-3 quote-v1-inner text-right">
            <!-- <a class="btn-transparent" href="http://www.keenthemes.com/preview/index.php?theme=metronic_admin"
                target="_blank"><i class="icon-rocket margin-right-10"></i>Preview Admin</a>-->
            <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-rocket margin-right-10')) . 'Preview Admin',
                array('controller' => '', 'action' => ''), array('class' => 'btn-transparent', 'escape' => false)); ?>
            <!--<a class="btn-transparent" href="http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469"><i class="icon-gift margin-right-10"></i>Purchase 2 in 1</a>-->
        </div>
    </div>
    <!-- END BLOCKQUOTE BLOCK -->

    <div class="clearfix"></div>

    <!-- BEGIN RECENT WORKS -->
    <div class="row recent-work margin-bottom-40">
        <div class="col-md-3">
            <h2><!--<a href="portfolio.html">Recent Works</a>-->
                <?php echo $this->html->link('Recent Works', array('controller' => '', 'action' => '')); ?>
            </h2>

            <p>Lorem ipsum dolor sit amet, dolore eiusmod quis tempor incididunt ut et dolore Ut veniam unde voluptatem.
                Sed unde omnis iste natus error sit voluptatem.</p>
        </div>
        <div class="col-md-9">
            <ul class="bxslider">
                <li>
                    <em>
                        <?php echo $this->html->image('/front/assets/img/works/img1.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img1.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #1',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>
                    </em>

                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>
                        <?php echo $this->html->image('/front/assets/img/works/img2.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img2.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #2',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>
                    </em>
                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>

                        <?php echo $this->html->image('/front/assets/img/works/img3.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img3.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #3',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>

                    </em>
                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>
                        <?php echo $this->html->image('/front/assets/img/works/img4.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img4.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #4',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>

                    </em>
                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>
                        <?php echo $this->html->image('/front/assets/img/works/img5.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img5.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #5',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>

                    </em>
                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>
                        <?php echo $this->html->image('/front/assets/img/works/img6.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                                             <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img6.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #6',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>

                    </em>

                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>

                </li>
                <li>
                    <em>

                        <?php echo $this->html->image('/front/assets/img/works/img3.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img3.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #3',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>

                    </em>
                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
                <li>
                    <em>

                        <?php echo $this->html->image('/front/assets/img/works/img4.jpg', array('alt' => '')); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-link icon-hover icon-hover-1')),
                            array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                        <?php echo $this->html->link($this->html->tag('i', '', array('class' => 'icon-search icon-hover icon-hover-2')),
                            array('controller' => '', 'action' => '/front/assets/img/works/img4.jpg'), array('class' => 'fancybox-button'
                            , 'title' => 'Project Name #4',
                                'data-rel' => 'fancybox-button', 'escape' => false)); ?>


                    </em>

                    <?php echo $this->html->link('<strong>Amazing Project</strong>
                        <b>Agenda corp.</b>', array('controller' => '', 'action' => ''), array('class' => 'bxslider-block', 'escape' => false)); ?>
                </li>
            </ul>
        </div>
    </div>
    <!-- END RECENT WORKS -->

    <div class="clearfix"></div>

    <!-- BEGIN TABS AND TESTIMONIALS -->
    <div class="row mix-block">
        <!-- TABS -->
        <div class="col-md-7 tab-style-1 margin-bottom-20">
            <ul class="nav nav-tabs">
                <li class="active"><!--<a href="#tab-1" data-toggle="tab">Multipurpose</a>-->
                    <?php echo $this->html->link('Multipurpose', array('controller' => '', 'action' => '#tab-1'),
                        array('data-toggle' => 'tab')); ?>
                </li>
                <li><!--<a href="#tab-2" data-toggle="tab">Documented</a>-->
                    <?php echo $this->html->link('Documented', array('controller' => '', 'action' => '#tab-2'),
                        array('data-toggle' => 'tab')); ?>
                </li>
                <li><!--<a href="#tab-3" data-toggle="tab">Responsive</a>-->
                    <?php echo $this->html->link('Responsive', array('controller' => '', 'action' => '#tab-3'),
                        array('data-toggle' => 'tab')); ?>
                </li>
                <li><!--<a href="#tab-4" data-toggle="tab">Clean & Fresh</a>-->
                    <?php echo $this->html->link('Clean & Fresh', array('controller' => '', 'action' => '#tab-4'),
                        array('data-toggle' => 'tab')); ?>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane row fade in active" id="tab-1">
                    <div class="col-md-3">
                        <?php echo $this->html->link($this->html->image('/front/assets/img/photos/img7.jpg',
                            array('class' => 'img-responsive', 'alt' => '')),
                            array('controller' => '', 'action' => '/front/assets/img/photos/img7.jpg'),
                            array('escape' => false, 'class' => 'fancybox-button', 'title' => 'Image Title', 'data-rel' => 'fancybox-button')); ?>
                    </div>
                    <div class="col-md-9">
                        <p class="margin-bottom-10">Raw denim you probably haven't heard of them jean shorts Austin.
                            Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor,
                            williamsburg carles vegan helvetica. Cosby sweater eu banh mi, qui irure terry richardson ex
                            squid Aliquip placeat salvia cillum iphone.</p>

                        <p><a class="more" href="#">Read more <i class="icon-angle-right"></i></a></p>
                    </div>
                </div>
                <div class="tab-pane row fade" id="tab-2">
                    <div class="col-md-9">
                        <p>Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid.
                            Exercitation +1 labore velit, blog sartorial PBR leggings next level wes anderson artisan
                            four loko farm-to-table craft beer twee. Qui photo booth letterpress, commodo enim craft
                            beer mlkshk aliquip jean shorts ullamco ad vinyl cillum PBR. Homo nostrud organic, assumenda
                            labore aesthetic magna delectus mollit. Keytar helvetica VHS salvia..</p>
                    </div>
                    <div class="col-md-3">
                        <!--<a href="/assets/img/photos/img10.jpg" class="fancybox-button" title="Image Title" data-rel="fancybox-button">
                            <img class="img-responsive" src="/assets/img/photos/img10.jpg" alt="" />
                        </a>-->
                        <?php echo $this->html->link($this->html->image('/front/assets/img/photos/img10.jpg',
                            array('class' => 'img-responsive', 'alt' => '')), array('controller' => '', 'action' => '/front/assets/img/photos/img10.jpg'),
                            array('class' => 'fancybox-button', 'title' => 'Image Title', 'data-rel' => 'fancybox-button', 'escape' => false)); ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="tab-3">
                    <p>Etsy mixtape wayfarers, ethical wes anderson tofu before they sold out mcsweeney's organic lomo
                        retro fanny pack lo-fi farm-to-table readymade. Messenger bag gentrify pitchfork tattooed craft
                        beer, iphone skateboard locavore carles etsy salvia banksy hoodie helvetica. DIY synth PBR
                        banksy irony. Leggings gentrify squid 8-bit cred pitchfork. Williamsburg banh mi whatever
                        gluten-free, carles pitchfork biodiesel fixie etsy retro mlkshk vice blog. Scenester cred you
                        probably haven't heard of them, vinyl craft beer blog stumptown. Pitchfork sustainable tofu
                        synth chambray yr.</p>
                </div>
                <div class="tab-pane fade" id="tab-4">
                    <p>Trust fund seitan letterpress, keytar raw denim keffiyeh etsy art party before they sold out
                        master cleanse gluten-free squid scenester freegan cosby sweater. Fanny pack portland seitan
                        DIY, art party locavore wolf cliche high life echo park Austin. Cred vinyl keffiyeh DIY salvia
                        PBR, banh mi before they sold out farm-to-table VHS viral locavore cosby sweater. Lomo wolf
                        viral, mustache readymade thundercats keffiyeh craft beer marfa ethical. Wolf salvia freegan,
                        sartorial keffiyeh echo park vegan.</p>
                </div>
            </div>
        </div>
        <!-- END TABS -->

        <!-- TESTIMONIALS -->
        <div class="col-md-5 testimonials-v1">
            <div id="myCarousel" class="carousel slide">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <span class="testimonials-slide">Denim you probably haven't heard of. Lorem ipsum dolor met consectetur adipisicing sit amet, consectetur adipisicing elit, of them jean shorts sed magna aliqua. Lorem ipsum dolor met consectetur adipisicing sit amet do eiusmod dolore.</span>

                        <div class="carousel-info">
                            <!--<img class="pull-left" src="assets/img/people/img1-small.jpg" alt="" />-->
                            <?php echo $this->html->image('/front/assets/img/people/img1-small.jpg', array('class' => 'pull-left', 'alt' => '')); ?>
                            <div class="pull-left">
                                <span class="testimonials-name">Lina Mars</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="testimonials-slide">Raw denim you Mustache cliche tempor, williamsburg carles vegan helvetica probably haven't heard of them jean shorts austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</span>

                        <div class="carousel-info">
                            <!--<img class="pull-left" src="assets/img/people/img5-small.jpg" alt="" />-->
                            <?php echo $this->html->image('/front/assets/img/people/img5-small.jpg', array('class' => 'pull-left', 'alt' => '')); ?>
                            <div class="pull-left">
                                <span class="testimonials-name">Kate Ford</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <span class="testimonials-slide">Reprehenderit butcher stache cliche tempor, williamsburg carles vegan helvetica.retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid Aliquip placeat salvia cillum iphone.</span>

                        <div class="carousel-info">
                            <!--<img class="pull-left" src="assets/img/people/img2-small.jpg" alt="" />-->
                            <?php echo $this->html->image('/front/assets/img/people/img2-small.jpg', array('class' => 'pull-left', 'alt' => '')); ?>
                            <div class="pull-left">
                                <span class="testimonials-name">Jake Witson</span>
                                <span class="testimonials-post">Commercial Director</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Carousel nav -->
                <!-- <a class="left-btn" href="#myCarousel" data-slide="prev"></a>-->
                <?php echo $this->html->link('', array('controller' => '', 'action' => '#myCarousel'), array('class' => 'left-btn', 'data-slide' => 'prev')); ?>
                <?php echo $this->html->link('', array('controller' => '', 'action' => '#myCarousel'), array('class' => 'right-btn', 'data-slide' => 'next')); ?>
                <!--  <a class="right-btn" href="" data-slide="next"></a>-->
            </div>
        </div>
        <!-- END TESTIMONIALS -->
    </div>
    <!-- END TABS AND TESTIMONIALS -->

    <!-- BEGIN STEPS -->
    <div class="row no-space-steps margin-bottom-40">
        <div class="col-md-4 col-sm-4">
            <div class="front-steps front-step-one">
                <h2>Goal definition</h2>

                <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="front-steps front-step-two">
                <h2>Analyse</h2>

                <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
            </div>
        </div>
        <div class="col-md-4 col-sm-4">
            <div class="front-steps front-step-three">
                <h2>Implementation</h2>

                <p>Lorem ipsum dolor sit amet sit consectetur adipisicing eiusmod tempor.</p>
            </div>
        </div>
    </div>
    <!-- END STEPS -->

    <!-- BEGIN CLIENTS -->
    <div class="row margin-bottom-40 our-clients">
        <div class="col-md-3">
            <h2><!--<a href="#">Our Clients</a>-->
                <?php echo $this->html->link('Our Clients', array('controller' => '', 'action' => '')); ?>
            </h2>

            <p>Lorem dipsum folor margade sitede lametep eiusmod psumquis dolore.</p>
        </div>
        <div class="col-md-9">
            <ul class="bxslider1 clients-list">
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_1_gray.png" alt="" />
                        <img src="assets/img/clients/client_1.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_1_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_1.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_2_gray.png" alt="" />
                        <img src="assets/img/clients/client_2.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_2_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_2.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_3_gray.png" alt="" />
                        <img src="assets/img/clients/client_3.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_3_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_3.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_4_gray.png" alt="" />
                        <img src="assets/img/clients/client_4.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_4_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_4.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_5_gray.png" alt="" />
                        <img src="assets/img/clients/client_5.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_5_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_5.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!-- <a href="#">
                         <img src="assets/img/clients/client_6_gray.png" alt="" />
                         <img src="assets/img/clients/client_6.png" class="color-img" alt="" />
                     </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_6_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_6.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>

                </li>
                <li>
                    <!--<a href="#">
                        <img src="assets/img/clients/client_7_gray.png" alt="" />
                        <img src="assets/img/clients/client_7.png" class="color-img" alt="" />
                    </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_7_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_7.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                </li>
                <li>
                    <!--  <a href="#">
                          <img src="assets/img/clients/client_8_gray.png" alt="" />
                          <img src="assets/img/clients/client_8.png" class="color-img" alt="" />
                      </a>-->
                    <?php echo $this->html->link($this->html->image('/front/assets/img/clients/client_8_gray.png',
                            array('alt' => '')) . $this->html->image('/front/assets/img/clients/client_8.png',
                            array('class' => 'color-img', 'alt' => '')), array('controller' => '', 'action' => ''), array('escape' => false)); ?>
                </li>
            </ul>
        </div>
    </div>
    <!-- END CLIENTS -->
</div>